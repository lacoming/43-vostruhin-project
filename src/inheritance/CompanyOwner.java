package inheritance;

public class CompanyOwner extends Worker{
    private String companyName;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public CompanyOwner(String name, String lastName, String profession, String companyName) {
        super(name, lastName, profession);
        this.companyName = companyName;
    }

    @Override
    public void goToWork() {
        System.out.println(super.getLastName() + " " + super.getName() + " " + super.getProfession() + " " +
                        this.companyName + ", начинает работать, а именно: ");
        System.out.println("Полетел в Майями договариваться о новой сделке для своей компании " + "\"" +
                this.companyName + "\"");
    }

    @Override
    public void goToVacation(int days) {
        System.out.println(super.getLastName() + " " + super.getName() + " " + super.getProfession() + " " +
                this.companyName + ", уходит в отпуск на " + days + " дней, и ");
        System.out.println("будет генерировать новые идеи по развитию " + this.companyName);
    }
}
