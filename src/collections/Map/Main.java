package collections.Map;

import java.util.HashMap;
import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String str;
        String delimeter = " ";
        HashMap<String, Integer> hashMap = new HashMap<>();

        System.out.println("Введите фразу: ");
        str = scanner.nextLine();

        String[] strings = str.split(delimeter);

        for (int i = 0; i < strings.length; i++) {
            if (hashMap.containsKey(strings[i])) {
                int temp = hashMap.get(strings[i]);
                temp++;
                hashMap.put(strings[i], temp);
            } else {
                hashMap.put(strings[i], 1);
            }
        }

        for (String key : hashMap.keySet()) {
            System.out.println(key + " - " + hashMap.get(key));
        }


    }


}
