package interfaces;

//У класса ТВШоу есть пока только имя и конструктор, в дальнейшем можно добавить поля "Время вещания", "Приоритет",
// "Возрастное ограничение" и т.д.
public class TVShow {

    private String showName;

    public TVShow(String showName) {
        this.showName = showName;
    }

    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
        this.showName = showName;
    }

}
