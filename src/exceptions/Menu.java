package exceptions;

import java.util.Scanner;

public class Menu {
    private boolean check = true;
    private int number;
    private int id;
    private int powerEngine;
    private String brand;
    private String model;
    private String color;
    private Car car;
    private CarDataBase carDataBase = new CarDataBase();

    public void showMenu() {

        while (check) {
            System.out.println("1. Добавить автомобиль в Базу Данных");
            System.out.println("2. Найти автомобиль в БД по ID");
            System.out.println("3. Показать список всех автомобилей в БД");
            System.out.println("4. Удалить автомобиль из БД");
            System.out.println("5. Изменить данные об автомобиле");
            System.out.println("0. Выход\n");
            System.out.println("Выберите нужный пункт меню и введите его цифру: ");

            Scanner scanner = new Scanner(System.in);

            number = scanner.nextInt();
            scanner.nextLine();

            switch (number) {
                case 1:
                    System.out.println("Введите марку автомобиля: ");
                    brand = scanner.nextLine();
                    System.out.println("Введите модель автомобиля: ");
                    model = scanner.nextLine();
                    System.out.println("Введите мощность двигателя: ");
                    powerEngine = scanner.nextInt();
                    scanner.nextLine();
                    System.out.println("Введите цвет автомобиля: ");
                    color = scanner.nextLine();
                    carDataBase.addCarToDataBase(brand, model, color, powerEngine);
                    break;
                case 2:
                    System.out.println("Введите ID автомобиля: ");
                    id = scanner.nextInt();
                    car = carDataBase.findCar(id);
                    if(car != null){
                        System.out.println("С таким ID найден автомобиль: \n");
                        System.out.println(car);
                    } else
                        System.out.println("Автомобиль с данным ID не найден\n");
                    break;
                case 3:
                    carDataBase.showInfoAllCars();
                    break;
                case 4:
                    System.out.println("Введите ID автомобиля, который хотите удалить");
                    id = scanner.nextInt();
                    car = carDataBase.findCar(id);
                    if(car != null){
                        carDataBase.removeCarFromDataBase(car);
                        System.out.println("Автомобиль " + car + " был удалён\n");
                    } else
                        System.out.println("Автомобиль с данным ID не найден\n");
                    break;
                case 5:
                    System.out.println("Введите ID атомобиля в который хотите внести изменения: ");
                    id = scanner.nextInt();
                    car = carDataBase.findCar(id);
                    if(car != null){
                        System.out.println("Выберите пункт, который хотите изменить в автомобиле: ");
                        while (check){
                            System.out.println("1. Изменить Марку автомобиля");
                            System.out.println("2. Изменить Модель автомобиля");
                            System.out.println("3. Изменить цвет автомобиля");
                            System.out.println("4. Изменить мощность автомобиля");
                            number = scanner.nextInt();
                            scanner.nextLine();
                            switch (number){
                                case 1:
                                    System.out.println("Введите новое название марки автомобиля: ");
                                    brand = scanner.nextLine();
                                    carDataBase.setCarBrandToArrayList(id,brand);
                                    carDataBase.arrayWriteToFile();
                                    System.out.println("Автомобил изменён: \n");
                                    System.out.println(car);
                                    check = false;
                                    break;
                                case 2:
                                    System.out.println("Введите новое имя модели автомобиля: ");
                                    model = scanner.nextLine();
                                    carDataBase.setCarModelToArrayList(id,model);
                                    carDataBase.arrayWriteToFile();
                                    System.out.println("Автомобил изменён: \n");
                                    System.out.println(car);
                                    check = false;
                                    break;
                                case 3:
                                    System.out.println("Введите новый цвет автомобиля: ");
                                    color = scanner.nextLine();
                                    carDataBase.setCarColorToArrayList(id,color);
                                    carDataBase.arrayWriteToFile();
                                    System.out.println("Автомобил изменён: \n");
                                    System.out.println(car);
                                    check = false;
                                    break;
                                case 4:
                                    System.out.println("Введите новую мощность автомобиля: ");
                                    powerEngine = scanner.nextInt();
                                    carDataBase.setCarPowerEngineToArrayList(id,powerEngine);
                                    carDataBase.arrayWriteToFile();
                                    System.out.println("Автомобил изменён: \n");
                                    System.out.println(car);
                                    check = false;
                                    break;
                                default:
                                    System.out.println("Номер не найден, повторите свой выбор\n");
                            }
                        }
                        check = true;
                    } else
                        System.out.println("Автомобиль с данным ID не найден\n");
                    break;

                case 0:
                    scanner.close();
                    check = false;
                    break;

                default:
                    System.out.println("Номер не найден, повторите свой выбор\n");
            }
        }
    }
}
