package inheritance;

public class Actor extends Worker{
    public Actor(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Пошел на сцену воздействовать на души зрителей");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("начнёт получать новый жизненный опыт, чтобы стать более многогранным актёром");
    }
}
