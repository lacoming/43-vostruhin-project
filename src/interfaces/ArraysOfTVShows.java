package interfaces;

import java.util.Random;

// Класс был создан как замена подключаемых библиотек, в котором хранятся массивы с названиями ТВШоу
public class ArraysOfTVShows {
    private String[] arrayOfNews = new String[]{
            "Самые шокирующие гипотезы", "С бодрым утром!", "Минтранс", "Самая полезная программа",
            "\"Военная тайна\" с Игорем Прокопенко", "СОВБЕЗ", "Грозовые ворота", "Слово пастыря",
            "Новости (с субтитрами)", "Поехали! Санкт-Петербург", "По семейным обстоятельствам",
            "Вечерние новости (с субтитрами)", "Время"};

    private String[] arrayOfEntertainment = new String[]{
            "Импровизация", "Однажды в России", "Битва экстрасенсов", "Музыкальная интуиция",
            "Женский стендап", "Выжить в Дубае", "Студия Союз", "Comedy Club", "Четыре свадьбы",
            "Пацанки", "Орёл и Решка"};

    private String[] arrayOfCartoons = new String[]{
            "Король Лев", "ВАЛЛ·И", "Как приручить дракона", "Песнь моря", "Балто", "Вверх", "Головоломка",
            "Корпорация монстров", "Рик и Морти", "Тролли", "Королевские каникулы", "Щенячий патруль",
            "Королевский корги", "Губка Боб", "Шрек 2", "Монстры на каникулах"
    };

    public String[] getArrayOfNews() {
        return arrayOfNews;
    }

    public void setArrayOfNews(String[] arrayOfNews) {
        this.arrayOfNews = arrayOfNews;
    }

    public String[] getArrayOfEntertainment() {
        return arrayOfEntertainment;
    }

    public void setArrayOfEntertainment(String[] arrayOfEntertainment) {
        this.arrayOfEntertainment = arrayOfEntertainment;
    }

    public String[] getArrayOfCartoons() {
        return arrayOfCartoons;
    }

    public void setArrayOfCartoons(String[] arrayOfCartoons) {
        this.arrayOfCartoons = arrayOfCartoons;
    }


    // Генерируем уникальную программу передач, из трёх массивов по такой формуле: 5 Шоу из раздела "Новостей",
    // 5 Шоу из "Развлечений", и если остаётся ешё место, то заполняем эфир "Мультиками"
    public String[] getArrayOfTVShowsNamesAnyLength(int length) {
        String[] namesOfTVShows = new String[length];

        for (int i = 0; i < namesOfTVShows.length; i++) {
            int randomElement;
            if (i < 5) {
                randomElement = (int) Math.floor(Math.random() * arrayOfNews.length);
                namesOfTVShows[i] = arrayOfNews[randomElement];
            } else {
                if (i < 10) {
                    randomElement = (int) Math.floor(Math.random() * arrayOfCartoons.length);
                    namesOfTVShows[i] = arrayOfCartoons[randomElement];
                } else {
                    randomElement = (int) Math.floor(Math.random() * arrayOfEntertainment.length);
                    namesOfTVShows[i] = arrayOfEntertainment[randomElement];
                }
            }
        }

        // С помощью переменной класса Random перемешиваем ТВШоу, чтобы для каждого канала телесетка была уникальной
        Random rnd = new Random();
        for (int i = 0; i < namesOfTVShows.length; i++) {
            int index = rnd.nextInt(i + 1);
            String temp = namesOfTVShows[index];
            namesOfTVShows[index] = namesOfTVShows[i];
            namesOfTVShows[i] = temp;
        }

        return namesOfTVShows;
    }

}
