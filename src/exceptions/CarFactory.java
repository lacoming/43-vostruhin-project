package exceptions;

public class CarFactory {

    static int currentID = 0;


    public Car createCar(String brand, String model, String color, int powerEngine){
        Car car = new Car(currentID, brand, model, color, powerEngine);
        currentID++;
        System.out.println(car);
        return car;
    }

}
