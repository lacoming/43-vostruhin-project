package incapsulation;

public class Main {
    public static void main(String[] args) {

        Human people[] = new Human[(int) (Math.random() * 1000)];

        String[] names = new String[]{"Лев", "Николай", "Фёдор", "Владимир", "Борис", "Михаил", "Максим", "Сергей"};
        String[] lastNames = new String[]{"Гоголь", "Толстой", "Булгаков", "Достоевский", "Маяковский", "Рыжий",
                "Набоков", "Есенин"};

        for (int i = 0; i < people.length; i++) {
            int name = (int) Math.floor(Math.random() * names.length);
            int lastName = (int) Math.floor(Math.random() * lastNames.length);
            Human human = new Human(names[name], lastNames[lastName], (int) (1 + Math.random() * 100));
            people[i] = human;
        }

        boolean sorted = false;

        while (!sorted) {
            sorted = true;
            for (int i = 0; i < people.length; i++) {
                for (int j = i + 1; j < people.length; j++) {
                    if (people[i].getAge() > people[j].getAge()) {
                        Human temp = people[i];
                        people[i] = people[j];
                        people[j] = temp;
                        sorted = false;
                    }
                }
            }
        }

        for (Human person : people) {
            person.sayHello(person);
        }
    }
}
