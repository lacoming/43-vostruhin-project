package threads;

public class Factory {
    private FaceControl faceControl;

    public Factory() {
        faceControl = new FaceControl();
    }

    public void goToWork(Worker worker) {
        faceControl.checkPass(worker);
    }

}
