package exceptions;

import java.io.*;
import java.util.ArrayList;

public class CarDataBase extends CarFactory implements Serializable {
    private File file = new File("carDataBase.out");

    private ArrayList<Car> carArrayList = new ArrayList<>();

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public ArrayList<Car> getCarArrayList() {
        return carArrayList;
    }

    public void setCarBrandToArrayList(int id, String brand) {
        for (Car element : carArrayList){
            if(element.getId() == id){
                element.setBrand(brand);
            }
        }
    }

    public void setCarModelToArrayList(int id, String model) {
        for (Car element : carArrayList){
            if(element.getId() == id){
                element.setModel(model);
            }
        }
    }

    public void setCarColorToArrayList(int id, String color) {
        for (Car element : carArrayList){
            if(element.getId() == id){
                element.setColor(color);
            }
        }
    }

    public void setCarPowerEngineToArrayList(int id, int powerEngine) {
        for (Car element : carArrayList){
            if(element.getId() == id){
                element.setPowerEngine(powerEngine);
            }
        }
    }

    // При создании Базы Данных, автоматически проверяется существует ли файл, если нет то он создается
    public CarDataBase() {
        if (!file.exists()) {
            createCarDataBase();
        } else {
            arrayReadFromFile();
            if(carArrayList.size() > 0) {
                int i = carArrayList.size() - 1;
                currentID = carArrayList.get(i).getId() + 1;
                System.out.println("Загрузка БД прошла успешно");
            }
        }
    }

    public void arrayReadFromFile() {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));

            while (true) {
                try {
                    carArrayList.add((Car) objectInputStream.readObject());
                } catch (EOFException e) {
                    break;
                }
            }
            objectInputStream.close();

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.getMessage();
        } catch (IOException e) {
            e.getMessage();
        } catch (ClassNotFoundException e) {
            e.getMessage();
        }
    }

    public void arrayWriteToFile() {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file));

            for (Car car : carArrayList) {
                objectOutputStream.writeObject(car);
            }

            objectOutputStream.close();

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.getMessage();
        } catch (IOException e) {
            e.getMessage();
        }
    }

    private void createCarDataBase() {
        try {
            System.out.println("Создан новый файл БД\n");
            file.createNewFile();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addCarToDataBase(String brand, String model, String color, int powerEngine) {
        Car car = createCar(brand, model, color, powerEngine);
        this.carArrayList.add(car);
        arrayWriteToFile();
    }

    public void showInfoAllCars() {
        if(carArrayList.size() > 0) {
            for (Car car : carArrayList) {
                System.out.println(car);
            }
        } else
            System.out.println("В данный момент список пуст\n");
    }

    public Car findCar(int id){
        for(Car car : carArrayList){
            if(car.getId() == id){
                return car;
            }
        }
        return null;
    }

    public void removeCarFromDataBase(Car car) {
        this.carArrayList.remove(car);
        arrayWriteToFile();
    }

    public Car changeCar(Car car) {

        return null;
    }


}

