package threads;

import java.util.concurrent.Semaphore;

public class FaceControl {
    private Semaphore semaphore = new Semaphore(2);

    public void checkPass(Worker worker) {
        worker.setSemaphore(this.semaphore);
        worker.start();
    }
}
