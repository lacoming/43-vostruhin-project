package interfaces;

// В интерфейс по мимо заданных в ДЗ четырёх методов, создал ещё два
public interface TVRemote {

    void switchChannel(int channel);

    void channelForward();

    void channelBack();

    void backToLastChannel();

    // Настраивает "Базовый комплект" каналов на телевизор
    void tuneBasicChannels();

    // Включает случаный канал со случайным ТВШоу
    void randomChannel();

}