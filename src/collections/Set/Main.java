package collections.Set;

import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        boolean check = true;
        int temp;
        String value;
        String region;
        Scanner scanner = new Scanner(System.in);

        HashSet<Number> numbers = new HashSet<>();

        while (check) {
            System.out.println("Введите номер, который Вы хотите: ");
            value = scanner.nextLine();
            System.out.println("Введите Ваш регион: ");
            region = scanner.nextLine();

            Number number = new Number(value, region);

            if (numbers.add(number)) {
                System.out.println("Ваш номер зарегистрирован!\n");
            } else {
                System.out.println("Такой номер существует, введите другой\n");
            }

            System.out.println("Хотите выйти?");
            System.out.println("Да - 0");
            System.out.println("Нет - 1");
            temp = scanner.nextInt();
            scanner.nextLine();

            if (temp == 0) {
                check = false;
            }
        }
    }
}
