package inheritance;

public class Main {
    public static void main(String[] args) {
        Worker actor = new Actor("Александр", "Вострухин", "актёр");
        Worker companyOwner = new CompanyOwner("Илон", "Маск", "владелец компании",
                "SpaceX");
        Worker programmer = new Programmer("Паша", "Дуров", "программист");
        Worker teacher = new Teacher("Иисус", "Христос", "учитель");



        actor.goToWork();
        System.out.println();

        companyOwner.goToWork();
        System.out.println();

        programmer.goToWork();
        System.out.println();

        teacher.goToWork();
        System.out.println();

        actor.goToVacation(56);
        System.out.println();

        companyOwner.goToVacation(364);
        System.out.println();

        programmer.goToVacation(14);
        System.out.println();

        teacher.goToVacation(28);
    }
}
