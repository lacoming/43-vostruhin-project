package inheritance;

public class Programmer extends Worker{
    public Programmer(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Размял пальцы, и сел дописывать эту программу");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("будет перечитывать книгу \"Грокаем алгоритмы\", попивая коктейль на берегу Индийского " +
                "океана");
    }
}
