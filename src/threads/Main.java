package threads;

public class Main {
    public static void main(String[] args) {

        Factory factory = new Factory();

        Worker worker = new Worker("Настя");
        Worker worker1 = new Worker("Саша");
        Worker worker2 = new Worker("Коля");
        Worker worker3 = new Worker("Игнат");

        factory.goToWork(worker);
        factory.goToWork(worker1);
        factory.goToWork(worker2);
        factory.goToWork(worker3);
    }
}
