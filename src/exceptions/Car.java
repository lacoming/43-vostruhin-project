package exceptions;

import java.io.Serializable;

public class Car implements Serializable {
    private int id;
    private String brand;
    private String model;
    private String color;
    private int powerEngine;

    public Car(int id, String brand, String model, String color, int powerEngine) {
        this.id = id;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.powerEngine = powerEngine;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPowerEngine() {
        return powerEngine;
    }

    public void setPowerEngine(int powerEngine) {
        this.powerEngine = powerEngine;
    }

    @Override
    public String toString() {
        return  "Марка: \"" + this.brand + "\" \n" +
                "Модель: \"" + this.model + "\" \n" +
                "Цвет: " + this.color + "\n" +
                "Мощность двигателя: " + this.powerEngine + "л.с." + "\n" +
                "ID автомобиля: " + this.id + "\n";
    }

}
