package interfaces;

public class Main {
    public static void main(String[] args) {

        // Создаем телевизор
        TV tv = new TV();

        //Вызываем метод телевизора, показывающий меню
        tv.menu(tv);
    }
}
