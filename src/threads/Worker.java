package threads;

import java.util.concurrent.Semaphore;

public class Worker extends Thread {
    private String name;
    private Semaphore semaphore;

    public void setSemaphore(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    public Worker(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        try {
            System.out.println(name + " пришёл на завод");
            System.out.println(name + " ждёт пропуск\n");
            semaphore.acquire();
            System.out.println(name + " получил пропуск");
            Thread.sleep(1000);
            System.out.println(name + " работает");
            Thread.sleep(1000);
            semaphore.release();
            System.out.println(name + " отдал пропуск");
            System.out.println(name + " пошёл домой\n");
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
