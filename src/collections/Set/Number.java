package collections.Set;

import java.util.Objects;

public class Number {
    private String value;
    private String region;

    public Number(String value, String region) {
        this.value = value;
        this.region = region;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Number number = (Number) o;
        return value.equals(number.value) && region.equals(number.region);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, region);
    }
}
