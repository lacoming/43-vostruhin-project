package incapsulation;

public class Human {
    private String name;
    private String lastName;
    private int age;

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public Human() {
    }

    public Human(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    public void sayHello(Human human) {
        if (human.age > 4 && human.age < 21) {
            System.out.println("Меня зовут " + human.name + " " + human.lastName + " мне " + human.age + " лет");
        } else {

            int lastCharacter = human.age % 10;

            if (lastCharacter == 1) {
                System.out.println("Меня зовут " + human.name + " " + human.lastName + " мне " + human.age + " год");
            } else {
                if (lastCharacter > 1 && lastCharacter < 5) {
                    System.out.println("Меня зовут " + human.name + " " + human.lastName + " мне " + human.age + " года");
                } else {
                    System.out.println("Меня зовут " + human.name + " " + human.lastName + " мне " + human.age + " лет");
                }
            }
        }
    }
}
