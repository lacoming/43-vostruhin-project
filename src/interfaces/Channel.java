package interfaces;

// Создаем класс "Канал", и наследуем его от класса с массивами ТВШоу(а-ля примитивное подключение библиотек)
public class Channel extends ArraysOfTVShows {

    // У Канала есть его название и массив его шоу
    private String channelName;
    private TVShow[] tvShows;

    public Channel() {
        generateTVShows();
    }

    // При создании нового Канала автоматически генерируем ему сетку телевещания(чтобы как в жизни)
    public Channel(String channelName) {
        this.channelName = channelName;
        generateTVShows();
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public TVShow[] getTvShows() {
        return tvShows;
    }

    public void setTvShows(TVShow[] tvShows) {
        this.tvShows = tvShows;
    }

    // Метод, создающий массив со случайным колличеством(в диапазоне от 10 до 30) элементов, и заполняющий его
    // различными ТВШоу
    public TVShow[] generateTVShows() {

        String[] newChannel = new String[(int) (Math.random() * 30) + 10];
        this.tvShows = new TVShow[newChannel.length];

        // Метод, позволяющий заполнить массив Строк любым колличеством названий телепередач
        newChannel = getArrayOfTVShowsNamesAnyLength(newChannel.length);

        // Теперь через цикл создаем ТВШоу, присваеваем ему имя из массива Строк, и добавляем в массив
        for (int i = 0; i < tvShows.length; i++) {
            tvShows[i] = new TVShow(newChannel[i]);
        }

        return this.tvShows;
    }

    // Метод, который включает случайный канал со случайным ТВШоу
    public TVShow randomChannelShow(){
        int temp = (int) Math.floor(Math.random() * tvShows.length);
        return tvShows[temp];
    }
}
