package inheritance;

public class Teacher extends Worker{
    public Teacher(String name, String lastName, String profession) {
        super(name, lastName, profession);
    }

    @Override
    public void goToWork() {
        super.goToWork();
        System.out.println("Просвещает умы непросвещенных");
    }

    @Override
    public void goToVacation(int days) {
        super.goToVacation(days);
        System.out.println("поедет в уединённое место, где ученики не смогут его достать со своими вопросами");
    }
}
