package interfaces;

import java.util.ArrayList;
import java.util.Scanner;

public class TV implements TVRemote {
    // Создаем сразу телевизор с десятью массивами, в последствии, если телевизоры нужно будет создавать разные - от
    // разных производителей, то можно в сеттере добавить функционал
    private Channel[] channels = new Channel[10];

    // Создаем поле пульта, чтобы вызывать методы телевизора
    private TVRemote tvRemote;

    // Создаем временный канал, список истории просмотров(чтобы возвращаться к предыдущему просмотренному каналу,
    // и номер канала(чтобы класть или брать его из списка)
    private Channel currentChannel;
    private int numberOfCurrentChannel;
    private ArrayList<Integer> channelsHistory = new ArrayList<Integer>();


    public TVRemote getTvRemote() {
        return tvRemote;
    }

    public void setTvRemote(TVRemote tvRemote) {
        this.tvRemote = tvRemote;
    }

    public Channel[] getChannels() {
        return channels;
    }

    public void setChannels(Channel[] channels) {
        this.channels = channels;
    }

    @Override
    // Метод получает на входе номер канала на который пользователь хочет перейти, и меняет его на текущий канал,
    // а предыдущий номер канала отправляется в массив истории просмотра
    public void switchChannel(int num) {
        channelsHistory.add(numberOfCurrentChannel);
        numberOfCurrentChannel = num;
        if (num < channels.length && num >= 0) {
            currentChannel = channels[num];
            System.out.println("Сейчас на экране: ");
            System.out.println(currentChannel.getChannelName());

            // На данном этапе выбираем случайный канал, это можно исправить впоследствии, добавив временную сетку
            // и базы данных шоу каналов
            System.out.println(currentChannel.randomChannelShow().getShowName());
            System.out.println();
        } else {
            System.out.println("Извините канал с таким номером не найден, попробуйте ввести номер еще раз: ");
        }
    }

    @Override
    // Метод переключает на один канал вперед по списку, если это возможно, и добавляет элемент в массив Истории
    // просмотров
    public void channelForward() {
        if (numberOfCurrentChannel < channels.length - 1) {
            channelsHistory.add(numberOfCurrentChannel);
            numberOfCurrentChannel++;
            currentChannel = channels[numberOfCurrentChannel];
            System.out.println("Сейчас на экране: ");
            System.out.println(currentChannel.getChannelName());
            System.out.println(currentChannel.randomChannelShow().getShowName());
            System.out.println();
        } else {
            System.out.println("Это последний канал в списке");
        }
    }

    @Override
    // Метод переключает на один канал назад по списку, если это возможно, и добавляет элемент в массив Истории
    // просмотров
    public void channelBack() {
        if (numberOfCurrentChannel > 0) {
            channelsHistory.add(numberOfCurrentChannel);
            numberOfCurrentChannel--;
            currentChannel = channels[numberOfCurrentChannel];
            System.out.println("Сейчас на экране: ");
            System.out.println(currentChannel.getChannelName());
            System.out.println(currentChannel.randomChannelShow().getShowName());
            System.out.println();
        } else {
            System.out.println("Это первый канал в списке");
        }
    }

    @Override
    // Метод позволяет перейти на предыдущий просмотренный канал
    public void backToLastChannel() {
        if (channelsHistory.size() != 0) {
            // Список при удалении из массива Истории просмотров, удаляет последний элемент, перед этим вернув его
            // значение в переменную Текущего канала
            numberOfCurrentChannel = channelsHistory.remove(channelsHistory.size() - 1);
            currentChannel = channels[numberOfCurrentChannel];
            System.out.println("Сейчас на экране: ");
            System.out.println(currentChannel.getChannelName());
            System.out.println(currentChannel.randomChannelShow().getShowName());
            System.out.println();
        } else {
            System.out.println("Извините, история просмотров пуста");
        }
    }

    @Override
    // Метод заполняет массив каналов базовым набором
    public void tuneBasicChannels() {
        channels[0] = new Channel("1. Первый канал");
        channels[1] = new Channel("2. Россия 1");
        channels[2] = new Channel("3. 2Х2");
        channels[3] = new Channel("4. НТВ");
        channels[4] = new Channel("5. Рен-ТВ");
        channels[5] = new Channel("6. ТНТ");
        channels[6] = new Channel("7. Пятница");
        channels[7] = new Channel("8. СТС");
        channels[8] = new Channel("9. ТВ-3");
        channels[9] = new Channel("10. Домашний");
    }

    @Override
    // Метод включает случайный канал со случайным ТВШоу
    public void randomChannel() {
        if (channels != null) {
            int temp = (int) Math.floor(Math.random() * channels.length);
            System.out.println(channels[temp].getChannelName());

            // Вызываем имя через метод, который выдаёт случайное шоу из списка канала(можно добавить функционал,
            // который привяжет каналы к временной сетке
            System.out.println(channels[temp].randomChannelShow().getShowName());
            numberOfCurrentChannel = temp;
        } else {
            System.out.println("Каналы не найдены, возможно, нужно провести настройку вашего Телевизора");
        }
    }

    // Метод, выполнящий функции меню
    public void menu(TV tv) {

        // переменная для циклов
        boolean bool = true;

        // Создаём переменные для ввода данных пользователем
        Scanner in = new Scanner(System.in);
        int input;

        System.out.println("Вас приветсвует компания \"TELEFUNKEN\"");
        System.out.println("Каналы телепередач не найдены, хотите настроить базовый комплект?");
        System.out.println("1 - Да");
        System.out.println("0 - Нет");

        // Ввод пользователя, можно ещё сделать проверки на корректный ввод
        input = in.nextInt();

        if (input == 1) {

            // Вызываем метод, который загружает в телевизор базовый комплект, состоящий из 10 каналов
            // также каждый из 10 каналов, заполняется рандомными(в диапазоне от 10 до 30) ТВШоу, создавая
            // уникальную сетку под каждый канал. Сам набор каналов берется из трех массивов(Новости/Развлечения/
            // Мультики)
            tv.tuneBasicChannels();

            System.out.println("Базовый набор каналов успешно установлен!");
        }

        // Если массив каналов на телевизоре всё ещё отсутствует, даем еще один шанс установить Базовый комплект
        if (tv.getChannels() != null) {
            System.out.println("Сейчас на экране");

            // Выбираем рандомный канал и включаем на нем рандомное шоу(можно конвертировать, если появится временная
            // сетка
            tv.randomChannel();
            System.out.println();
        } else {
            while (bool) {
                System.out.println("Каналы не найдены, хотите настроить базовый комплект?");
                System.out.println("1 - Да");
                System.out.println("0 - Нет");

                input = in.nextInt();

                if (input == 1) {
                    tv.tuneBasicChannels();
                    System.out.println("Базовый набор каналов успешно установлен");
                    bool = false;
                }
                // Можно отказаться от этой идеи и устанавливать базовый комплект по умолчанию, либо сделать метод,
                // который позволит подключать другие комплекты каналов
                if (input == 0) {
                    System.out.println("Упс, дальше будет ошибка");
                    bool = false;
                }
            }
        }

        bool = true;

        // Создаем бесконечный цикл, чтобы меню было доступно каждый раз после нового выбора
        while (bool) {

            System.out.println("Список доступных каналов:");

            // Через цикл выводим полный список доступных каналов
            for (int i = 0; i < tv.getChannels().length; i++) {
                System.out.println(tv.getChannels()[i].getChannelName());
            }

            System.out.println("Чтобы переключиться на нужный канал, введите его номер");
            System.out.println("Чтобы переключиться на один канал вперед, введите \"0\"");
            System.out.println("Чтобы переключиться на один канал назад, введите \"-1\"");
            System.out.println("Чтобы переключиться на последний включенный канал введите \"-2\"");
            System.out.println("Введите \"11\", чтобы выйти из программы");
            System.out.println();

            input = in.nextInt();

            switch (input) {
                case 1:
                    // Метод меняющий текущий канал на новый, а также добавляет число в список channelsHistory,
                    // чтобы можно было вернуться на предыдущий канал
                    tv.switchChannel(0);
                    break;
                case 2:
                    tv.switchChannel(1);
                    break;
                case 3:
                    tv.switchChannel(2);
                    break;
                case 4:
                    tv.switchChannel(3);
                    break;
                case 5:
                    tv.switchChannel(4);
                    break;
                case 6:
                    tv.switchChannel(5);
                    break;
                case 7:
                    tv.switchChannel(6);
                    break;
                case 8:
                    tv.switchChannel(7);
                    break;
                case 9:
                    tv.switchChannel(8);
                    break;
                case 10:
                    tv.switchChannel(9);
                    break;
                case 0:
                    // Метод меняет текущий канал на следующий по списку, также добавляет число в список channelsHistory,
                    // чтобы можно было вернуться на предыдущий канал
                    tv.channelForward();
                    break;
                case -1:
                    // Метод меняет текущий канал на предыдущий в списке, также добавляет число в список channelsHistory,
                    // чтобы можно было вернуться на предыдущий канал
                    tv.channelBack();
                    break;
                case -2:
                    //Метод возвращает нас на предыдущий канал, который мы смотрели до этого
                    tv.backToLastChannel();
                    break;
                case 11:
                    bool = false;
                    break;
                default:
                    System.out.println("Номер команды не найден, попробуйте ещё раз");
                    break;
            }
        }
    }
}
