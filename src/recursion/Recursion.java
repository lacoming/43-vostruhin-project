package recursion;

public class Recursion {

        public static void main(String[] args) {
            System.out.println(findFibonacciSequenceWithoutRecursion(3));
        }

        public static int findFibonacciSequenceWithRecursion (int index) {
            if (index == 0){
                return 0;
            } if (index == 1) {
                return 1;
            } else {
                return findFibonacciSequenceWithRecursion(index - 2) + findFibonacciSequenceWithRecursion(index - 1);
            }
        }

        public static int findFibonacciSequenceWithoutRecursion (int index) {

            if(index == 0){
                return 0;
            } else if (index == 1){
                return 1;
            }

            int n0 = 1;
            int n1 = 1;
            int n2 = 0;

            for (int i = 3; i <= index; i++) {
                n2 = n0 + n1;
                n0 = n1;
                n1 = n2;
            }

            return n2;
        }

}
