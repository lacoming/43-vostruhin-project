package recursion;

public class Main {
    public static void main(String[] args) {

        int[] numbers = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int [] array = new int [numbers.length];
        int j = 0;

        for (int i = 0; i < numbers.length; i++){
            if (numbers[i] != 0){
                array[j] = numbers[i];
                j++;
            }
        }

        for (int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
    }
}